{
  const {
    html,
  } = Polymer;
  /**
    `<cells-luis-ruiz-ruiz-login>` Description.

    Example:

    ```html
    <cells-luis-ruiz-ruiz-login></cells-luis-ruiz-ruiz-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-luis-ruiz-ruiz-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLuisRuizRuizLogin extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-luis-ruiz-ruiz-login';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="cells-luis-ruiz-ruiz-login-styles cells-luis-ruiz-ruiz-login-shared-styles"></style>
      <slot></slot>
      
          <p>[[t('cells-luis-ruiz-ruiz-login-welcome', 'Welcome to')]] Cells</p>
      
      `;
    }
  }

  customElements.define(CellsLuisRuizRuizLogin.is, CellsLuisRuizRuizLogin);
}